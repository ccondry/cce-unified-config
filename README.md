# cce-unified-config
JavaScript library for Cisco Contact Center Enterprise Unified Config APIs

## Usage
```js
const client = require('cce-unified-config')

let cce = new client({
  host: 'ccedata.dcloud.cisco.com',
  username: 'administrator@dcloud.cisco.com',
  password: 'C1sco12345'
})

cce.agent.changeDeskSetting('41377', '5000')

cce.list('agent', '311')
.then(results => {
  console.log(results)
}).catch(error => {
  console.error(error)
})
```
