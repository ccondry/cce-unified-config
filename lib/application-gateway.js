// https://ccedata.dcloud.cisco.com/unifiedconfig/config/applicationgateway?summary=true

async list(agentId, settingsId) {
  const agent = await this.findAgent(agentId)
  // set desk setting refUrl
  agent.agentDeskSettings = {
    refURL: this.url + 'applicationgateway' + '/'
  }
  await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
  console.log('agent desk settings changed')
  return agent
}

async get(agentId, settingsId) {
  const agent = await this.findAgent(agentId)
  // set desk setting refUrl
  agent.agentDeskSettings = {
    refURL: this.url + 'applicationgateway' + '/' + settingsId
  }
  await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
  console.log('agent desk settings changed')
  return agent
}
