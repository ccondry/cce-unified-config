const request = require('request-promise-native')
const axios = require('axios')
const parseXmlString = require('./parse-xml')

class UnifiedConfig {
  constructor({host, username, password}) {
    this.host = host
    this.url = `https://${host}/unifiedconfig/config/`
    this.auth = {
      username,
      password
    }
  }

  async changeDeskSetting(agentId, settingsId) {
    const agent = await this.findAgent(agentId)
    // set desk setting refUrl
    agent.agentDeskSettings = {
      refURL: this.url + 'agentdesksetting' + '/' + settingsId
    }
    await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
    console.log('agent desk settings changed')
    return agent
  }

  async changePassword(agentId, password) {
    const agent = await this.findAgent(agentId)
    // set desk setting refUrl
    agent.person.password = password
    await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
    console.log('agent password changed')
    return agent
  }

  async changeFirstName(id, data) {
    const agent = await this.findAgent(id)
    // set desk setting refUrl
    agent.person.firstName = data
    // update agent
    await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
    console.log('agent password changed')
    return agent
  }

  async changeLastName(id, data) {
    const agent = await this.findAgent(id)
    // set desk setting refUrl
    agent.person.lastName = data
    // update agent
    await axios.put('https://' + this.host + agent.refURL, agent, {auth: this.auth})
    console.log('agent password changed')
    return agent
  }

  async findAgent(agentId) {
    const params = {q: agentId}
    const response = await axios.get(this.url + 'agent', {auth: this.auth, params})
    try {
      const agent = response.data.agents[0].agent.find(v => {
        return v.agentId === agentId
      })
      return agent
    } catch (e) {
      throw {
        status: 404,
        statusText: `could not find agent with that exact ID (${agentId})`
      }
    }
  }

  async get(type, id) {
    try {
      const response = await request({
        url: `${this.url}${type.toLowerCase()}/${id}`,
        headers: {
          'Accept': 'application/xml'
        },
        auth: {
          user: this.auth.username,
          pass: this.auth.password
        }
      })
      const jsonData = await parseXmlString(response)
      // console.log('jsonData', jsonData)
      return jsonData[type]
    } catch (e) {
      throw e
    }
  }

  async modify(type, id, body) {
    try {
      const url = `${this.url}${type.toLowerCase()}/${id}`
      console.log('trying to PUT url', url)
      const response = await axios.put(url, body, {auth: this.auth})
      // console.log(response)
      return response.data
    } catch (e) {
      throw e
    }
  }

  async create(type, body) {
    try {
      const url = this.url + type.toLowerCase()
      const response = await axios.post(url, body, {auth: this.auth})
      // console.log('cce-unified-config create success response:', response)
      return response.headers.location
    } catch (e) {
      throw e
    }
  }

  async delete (type, id) {
    try {
      const url = `${this.url}${type.toLowerCase()}/${id}`
      return axios.delete(url, {auth: this.auth})
      // console.log('cce-unified-config create success response:', response)
      // return
    } catch (e) {
      throw e
    }
  }

  async list(type, q) {
    // pass params from input, and set max results
    const params = {
      q,
      resultsPerPage: 100,
      startIndex: 0
    }
    const headers = {
      // 'Content-Type'
      // don't change this to application/json because some types like precisionqueue
      // will return a 406
      'Accept': 'application/xml'
    }
    const options = {
      auth: this.auth,
      params,
      headers
    }
    try {
      const response = await axios.get(this.url + type.toLowerCase(), options)
      const jsonData = (await parseXmlString(response.data)).results
      // console.log('json',jsonData)
      let totalResults = jsonData.pageInfo.totalResults
      // console.log('totalResults', totalResults)
      // console.log('data', jsonData[`${type}s`][0][`${type}`])
      // good response?
      // console.log('jsonData', jsonData)
      if (totalResults === '0') {
        // no results
        return []
      } else if (totalResults === '1') {
        // return single result as an array of 1 element
        return [jsonData[`${type}s`][`${type}`]]
      } else {
        // 2 or more results
        let data = jsonData[`${type}s`][`${type}`]
        // console.log('data', data)
        while (totalResults > params.startIndex + 100) {
          // go get more results
          // increase cursor
          params.startIndex += 100
          // fetch next data set
          const response2 = await axios.get(this.url + type.toLowerCase(), options)
          const jsonData2 = (await parseXmlString(response2.data)).results
          // append data
          data = data.concat(jsonData2[`${type}s`][`${type}`])
        }
        // all done, return data as an array
        return data
      }
    } catch (e) {
      console.log(e.message)
      throw e
    }
  }
}

module.exports = UnifiedConfig
