const cceClient = require('../index')

let cce = new cceClient({
  host: 'ccedata.dcloud.cisco.com',
  username: 'administrator@dcloud.cisco.com',
  password: 'C1sco12345'
})

describe('getDialedNumbers()', function () {
  it('should list CCE dialed numbers', function (done) {
    cce.list('dialedNumber', '377')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('getCallTypes()', function () {
  it('should list CCE call types', function (done) {
    cce.list('callType', 'ccondry')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('getPrecisionQueues()', function () {
  it('should list CCE precision queues', function (done) {
    cce.list('precisionQueue', 'departments:(ccondry)')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('getSkillGroups()', function () {
  it('should list CCE skill groups', function (done) {
    // increase timeout for pulling large data set
    this.timeout(4000)
    cce.list('skillGroup', 'Cumulus')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('list agents', function () {
  it('should list CCE agents matching query', function (done) {
    // increase timeout for pulling large data set
    // this.timeout(4000)
    cce.list('agent', '377')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

let mrd
describe('Get Media Rouding Domain for Outbound Campaign', function () {
  it('should get MRD for Cisco_Voice on CCE', function (done) {
    // set properties
    cce.list('mediaRoutingDomain')
    .then(results => {
      console.log('found MRDs:', results.length)
      // find Cisco_Voice MRD and store it
      mrd = results.find(v => {
        return v.name === 'Cisco_Voice'
      })
      done()
    }).catch(e => done(e))
  })
})

let ivrCallType
describe('Get Call Type for Outbound IVR Campaigns', function () {
  it('should get Call Type CXDemo_Outbound_IVR_Selector on CCE', function (done) {
    cce.list('callType')
    .then(results => {
      console.log('found Call Types:', results)
      // find call type and store it
      ivrCallType = results.find(v => {
        return v.name === 'CXDemo_Outbound_IVR_Selector'
      })
      done()
    }).catch(e => done(e))
  })
})

let agentCallType
describe('Get Call Type for Outbound Agent Campaigns', function () {
  it('should get Call Type CXDemo_Outbound_Agent on CCE', function (done) {
    cce.list('callType')
    .then(results => {
      console.log('found Call Types:', results)
      // find call type and store it
      agentCallType = results.find(v => {
        return v.name === 'CXDemo_Outbound_Agent'
      })
      done()
    }).catch(e => done(e))
  })
})

describe('Create Dialed Number for Outbound Campaign', function () {
  it('should create a dialed number on CCE', function (done) {
    // set properties
		dialedNumber.setDialedNumberString(dnString);
		dialedNumber.setDescription(description);
		dialedNumber.setRoutingType(routingType);
		// MRD
		ReferenceBean mrdRef = getReferenceBean(restClient, mrdName, MediaRoutingDomain.class, MediaRoutingDomainList.class);
		dialedNumber.setMediaRoutingDomain(mrdRef);
		// Call Type
		ReferenceBean callTypeRef = getReferenceBean(restClient, callTypeName, CallType.class, CallTypeList.class);
		dialedNumber.setCallType(callTypeRef);
		// create dialed number
		dialedNumber = restClient.createAndGetBean(dialedNumber);

    cce.create('dialedNumber', {
      dialedNumberString: 'ob_ccondry_3',
      description: '53377',
      routingType: '1',
      mediaRoutingDomain: {
        refURL: ''
      },
      callType: {
        refURL: ''
      },
      campaignPurposeType: 'ivrCampaign',
      skillGroupName: 'ob_ccondry_ivr2'
      // callType: ''
    })
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('Create Campaign', function () {
  it('should create a CCE outbound dialer campaign', function (done) {
    cce.create('campaign', {
      name: 'ob_ccondry_3',
      dialedNumber: '53377',
      ivrPorts: '1',
      dialingMode: 'PROGRESSIVEONLY',
      campaignPurposeType: 'ivrCampaign',
      skillGroupName: 'ob_ccondry_ivr2'
      // callType: ''
    })
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})


describe('getCampaigns()', function () {
  it('should list CCE outbound dialer campaigns', function (done) {
    cce.list('campaign', 'ccondry_')
    .then(results => {
      console.log('found', results.length)
      done()
    }).catch(e => done(e))
  })
})
